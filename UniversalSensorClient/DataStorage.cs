﻿using System;
using Windows.UI.Xaml;
using UniversalSensorClient;

namespace DataModel
{
    // Databindings und Aktualisierung der Sensordaten
    public class DataStorage : DependencyObject
    {
        MainPage rootPage = MainPage.Current;
        // Dependency Properties für Hub; Siehe MS Code-Sample XamlBind und https://msdn.microsoft.com/library/windows/apps/br242362
        #region TemperatureSensorValueDP
        public string TemperatureSensorValue
        {
            get { return (string)GetValue(TemperatureSensorDPProperty); }
            set { SetValue(TemperatureSensorDPProperty, value); }
        }
        private const string TemperatureSensorDPName = "TemperatureSensorValue";
        private static readonly DependencyProperty _TemperatureSensorDPProperty =
            DependencyProperty.Register(TemperatureSensorDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty TemperatureSensorDPProperty { get { return _TemperatureSensorDPProperty; } }
        #endregion
        #region TemperatureSensorStatusDP
        public string TemperatureSensorStatus
        {
            get { return (string)GetValue(TemperatureSensorStatusDPProperty); }
            set { SetValue(TemperatureSensorStatusDPProperty, value); }
        }
        private const string TemperatureSensorStatusDPName = "TemperatureSensorStatus";
        private static readonly DependencyProperty _TemperatureSensorStatusDPProperty =
            DependencyProperty.Register(TemperatureSensorStatusDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty TemperatureSensorStatusDPProperty { get { return _TemperatureSensorStatusDPProperty; } }
        #endregion
        #region TimeOfLastTemperatureDP
        public string TimeOfLastTemperature
        {
            get { return (string)GetValue(TimeOfLastTemperatureDPProperty); }
            set { SetValue(TimeOfLastTemperatureDPProperty, value); }
        }
        private const string TimeOfLastTemperatureDPName = "TimeOfLastTemperature";
        private static readonly DependencyProperty _TimeOfLastTemperatureDPProperty =
            DependencyProperty.Register(TimeOfLastTemperatureDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty TimeOfLastTemperatureDPProperty { get { return _TimeOfLastTemperatureDPProperty; } }
        #endregion
        #region HumiditySensorValueDP
        public string HumiditySensorValue
        {
            get { return (string)GetValue(HumiditySensorDPProperty); }
            set { SetValue(HumiditySensorDPProperty, value); }
        }
        private const string HumiditySensorDPName = "HumiditySensorValue";
        private static readonly DependencyProperty _HumiditySensorDPProperty =
            DependencyProperty.Register(HumiditySensorDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty HumiditySensorDPProperty { get { return _HumiditySensorDPProperty; } }
        #endregion
        #region HumiditySensorStatusDP
        public string HumiditySensorStatus
        {
            get { return (string)GetValue(HumiditySensorStatusDPProperty); }
            set { SetValue(HumiditySensorStatusDPProperty, value); }
        }
        private const string HumiditySensorStatusDPName = "HumiditySensorStatus";
        private static readonly DependencyProperty _HumiditySensorStatusDPProperty =
            DependencyProperty.Register(HumiditySensorStatusDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty HumiditySensorStatusDPProperty { get { return _HumiditySensorStatusDPProperty; } }
        #endregion     
        #region TimeOfLastHumidityDP
        public string TimeOfLastHumidity
        {
            get { return (string)GetValue(TimeOfLastHumidityDPProperty); }
            set { SetValue(TimeOfLastHumidityDPProperty, value); }
        }
        private const string TimeOfLastHumidityDPName = "TimeOfLastHumidity";
        private static readonly DependencyProperty _TimeOfLastHumidityDPProperty =
            DependencyProperty.Register(TimeOfLastHumidityDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty TimeOfLastHumidityDPProperty { get { return _TimeOfLastHumidityDPProperty; } }
        #endregion

        public double LowerHumidity { get; set; }
        public double UpperHumidity { get; set; }
        public double LowerTemperature { get; set; }
        public double UpperTemperature { get; set; }

        #region HumidityColorDP
        public string HumidityColor
        {
            get { return (string)GetValue(HumidityColorDPProperty); }
            set { SetValue(HumidityColorDPProperty, value); }
        }
        private const string HumidityColorDPName = "HumidityColor";
        private static readonly DependencyProperty _HumidityColorDPProperty =
            DependencyProperty.Register(HumidityColorDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty HumidityColorDPProperty { get { return _HumidityColorDPProperty; } }
        #endregion
        #region TemperatureColorDP
        public string TemperatureColor
        {
            get { return (string)GetValue(TemperatureColorDPProperty); }
            set { SetValue(TemperatureColorDPProperty, value); }
        }
        private const string TemperatureColorDPName = "TemperatureColor";
        private static readonly DependencyProperty _TemperatureColorDPProperty =
            DependencyProperty.Register(TemperatureColorDPName, typeof(string), typeof(DataStorage), new PropertyMetadata(""));
        public static DependencyProperty TemperatureColorDPProperty { get { return _TemperatureColorDPProperty; } }
        #endregion

        public DataStorage()
        {
            TemperatureSensorValue = "0.0";
            HumiditySensorValue = "0.0";
            TemperatureSensorStatus = "Offline";
            HumiditySensorStatus = "Offline";
            TimeOfLastTemperature = "Offline";
            TimeOfLastHumidity = "Offline";
        }

        #region SwitchSensorStatus
        // Änderung des Status auf Online/Offline
        public void SwitchTemperatureSensorStatus(string status)
        {
            if (status == "Online")
            {
                TemperatureSensorStatus = "Online";
            }
            else
            {
                TemperatureSensorStatus = "Offline";
            }
        }
        public void SwitchHumiditySensorStatus(string status)
        {
            if (status == "Online")
            {
                HumiditySensorStatus = "Online";
            }
            else
            {
                HumiditySensorStatus = "Offline";
            }
        }
        #endregion
        #region UpdateSensors
        // Werte für die Sensoren und Zeitstemple aktualisieren
        public void UpdateTemperatureSensor(string read)
        {
            if (TemperatureSensorStatus == "Offline")
            {
                SwitchTemperatureSensorStatus("Online");
            }

            // Wert aktualisieren über DependencyProperty
            TemperatureSensorValue = read + " °C";

            if (Convert.ToDouble(read) < LowerTemperature)
            {
                TemperatureColor = "Red";
            }
            else if (Convert.ToDouble(read) > UpperTemperature)
            {
                TemperatureColor = "Red";
            }
            else
            {
                TemperatureColor = "Green";
            }

            TimeOfLastTemperature = DateTime.Now.ToString();
        }
        public void UpdateHumiditySensor(string read)
        {
            if (HumiditySensorStatus == "Offline")
            {
                SwitchHumiditySensorStatus("Online");
            }

            // Wert aktualisieren über DependencyProperty
            HumiditySensorValue = read + " %";

            if (Convert.ToDouble(read) < LowerHumidity)
            {
                string temp = LowerHumidity.ToString();
                HumidityColor = "Red";
            }
            else if (Convert.ToDouble(read) > UpperHumidity)
            {
                string temp = UpperHumidity.ToString();
                HumidityColor = "Red";
            }
            else
            {
                HumidityColor = "Green";
            }

            TimeOfLastHumidity = DateTime.Now.ToString();
        }
        #endregion
        #region UpdateThresholds
        public void UpdateHumidityThresholds(double lower, double upper)
        {
            LowerHumidity = lower;
            UpperHumidity = upper;
            rootPage.localSettings.Values["lower_Humidity"] = LowerHumidity;
            rootPage.localSettings.Values["upper_Humidity"] = UpperHumidity;
        }
        public void UpdateTemperatureThresholds(double lower, double upper)
        {
            LowerTemperature = lower;
            UpperTemperature = upper;
            rootPage.localSettings.Values["lower_temperature"] = LowerTemperature;
            rootPage.localSettings.Values["upper_temperature"] = UpperTemperature;
        }
        #endregion
    }
}
