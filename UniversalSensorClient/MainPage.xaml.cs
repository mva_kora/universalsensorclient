﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.Storage;
using DataModel;
using Windows.UI.ViewManagement;

namespace UniversalSensorClient
{
    public sealed partial class MainPage : Page
    {
        #region Properties
        // Verweis auf die MainPage anlegen, damit andere Pages Methoden der MainPage aufrufen können
        public static MainPage Current;
        // Fenstertitel
        public const string FEATURE_NAME = "UniversalSensorClient";
        // Datenspeicher
        public DataStorage DataStorage;
        // Verbindungsinformationen anlegen
        public WsConnection WsConnection;
        // Verweis auf den StatusBlock, damit die Connection Klasse Updates senden kann (sonst: Marshalling Error)
        public TextBlock StatusBlockRef;
        // Verweis auf Verzeichnis der lokalen App-Einstellungen
        public StorageFolder localFolder;
        // Lokale App-Einstellungen
        public ApplicationDataContainer localSettings;
        #endregion
        #region Methods
        public MainPage()
        {
            // Default Initialisierung
            this.InitializeComponent();
            // Verweis auf die MainPage setzen
            Current = this;
            // Referenz auf Statusnotifikation
            StatusBlockRef = StatusBlock;
            // Datenspeicher initialisieren
            DataStorage = new DataStorage();
            // Setze Verbindungsinformationen
            WsConnection = new WsConnection();
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size { Height = 400, Width = 480 });
            // Teile dem Nutzer mit, dass keine Verbindung besteht
            NotifyUser("Keine Verbindung vorhanden", "error");
            // Get local app data
            localFolder = ApplicationData.Current.LocalFolder;
            localSettings = ApplicationData.Current.LocalSettings;  
            
            // Server-URL aus lokalen App-Einstellungen auslesen
            // Falls keine Einstellung vorhanden ist, lege sie neu an
            if (!localSettings.Values.ContainsKey("server_url"))
            {
                localSettings.Values["server_url"] = "ws://echo.websocket.org";
                NotifyUser("Standard-Adresse ws://echo.websocket.org geladen", "status");
            }
            if (!localSettings.Values.ContainsKey("lower_Humidity"))
            {
                localSettings.Values["lower_Humidity"] = 80;
            }
            if (!localSettings.Values.ContainsKey("upper_Humidity"))
            {
                localSettings.Values["upper_Humidity"] = 150;
            }
            if (!localSettings.Values.ContainsKey("lower_temperature"))
            {
                localSettings.Values["lower_temperature"] = 80;
            }
            if (!localSettings.Values.ContainsKey("upper_temperature"))
            {
                localSettings.Values["upper_temperature"] = 150;
            }
            // Server-URL in Connection-Klasse ablegen
            WsConnection.UriString = localSettings.Values["server_url"].ToString();
            DataStorage.LowerHumidity = Convert.ToDouble(localSettings.Values["lower_Humidity"]);
            DataStorage.UpperHumidity = Convert.ToDouble(localSettings.Values["upper_Humidity"]);
            DataStorage.LowerTemperature = Convert.ToDouble(localSettings.Values["lower_temperature"]);
            DataStorage.UpperTemperature = Convert.ToDouble(localSettings.Values["upper_temperature"]);

            // Rufe standardmäßig die Settings-Page auf
            this.MainFrame.Navigate(typeof(Settings));
        }

        public void NotifyUser(string strMessage, string messageType)
        {
            if ( messageType.CompareTo("status") == 0)
            {
                StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
            }
            else if ( messageType.CompareTo("warning") == 0 )
            {
                StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Yellow);
            }
            else
            {
                StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
            }
            
            StatusBlock.Text = strMessage;

            // StatusBlock einklappen, wenn kein Text vorhanden ist
            StatusBorder.Visibility = (StatusBlock.Text != String.Empty) ? Visibility.Visible : Visibility.Collapsed;
            if (StatusBlock.Text != String.Empty)
            {
                StatusBorder.Visibility = Visibility.Visible;
                StatusPanel.Visibility = Visibility.Visible;
            }
            else
            {
                StatusBorder.Visibility = Visibility.Collapsed;
                StatusPanel.Visibility = Visibility.Collapsed;
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            mysplitview1.IsPaneOpen = !mysplitview1.IsPaneOpen;  
        }
        private void UserSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            if ( listBox.SelectedIndex == 0 )
            {
                this.MainFrame.Navigate(typeof(Hub));
                mysplitview1.IsPaneOpen = false;
            }
            else if ( listBox.SelectedIndex == 1 )
            {
                this.MainFrame.Navigate(typeof(Settings));
                mysplitview1.IsPaneOpen = false;
            }
            else
            {
                return;
            }
        }
        #endregion
    }
}
