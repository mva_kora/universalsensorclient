﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Networking.Sockets;
using DataModel;

namespace UniversalSensorClient
{
    public sealed partial class Hub : Page
    {
        MainPage rootPage = MainPage.Current;
        public DataStorage DataStorage { get; set; }

        public Hub()
        {
            DataStorage = rootPage.DataStorage;
            this.InitializeComponent();            
            // DataContext für DependencyProperties setzen
            this.DataContext = DataStorage;            
        }
    }
}
