﻿using System;
using System.Threading.Tasks;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.Web;
using UniversalSensorClient;

namespace DataModel
{
    public class WsConnection
    {
        #region Properties
        MainPage rootPage = MainPage.Current;
        private string uriString;
        public string UriString
        {
            get { return uriString; }
            set { uriString = value; }
        }

        private bool connectionStatus;
        public bool ConnectionStatus
        {
            get { return connectionStatus; }
            set { connectionStatus = value; }
        }      
        private MessageWebSocket messageWebSocket;
        public MessageWebSocket MessageWebSocket
        {
            get { return messageWebSocket; }
            set { messageWebSocket = value; }
        }

        private DataWriter messageWriter;
        public DataWriter MessageWriter
        {
            get { return messageWriter; }
            set { messageWriter = value; }
        }
        #endregion
        #region Methods
        public WsConnection()
        {
            uriString = "";
            connectionStatus = false;
            messageWebSocket = null;
            messageWriter = null;
        }
        // Übersetze URI
        public bool TryGetUri(string uriString, out Uri uri)
        {
            uri = null;

            Uri webSocketUri;
            // Leerzeichen entfernen
            if (!Uri.TryCreate(uriString.Trim(), UriKind.Absolute, out webSocketUri))
            {
                rootPage.NotifyUser("Error: Invalid URI", "error");
                return false;
            }

            // Fragmente mit # entfernen.
            if (!String.IsNullOrEmpty(webSocketUri.Fragment))
            {
                rootPage.NotifyUser("Error: URI Fragmente werden in WebSocket-URIs nicht unterstützt", "error");
                return false;
            }

            // Uri.SchemeName gibt das "Schema" zurück (was vor dem :// steht) 
            if ((webSocketUri.Scheme != "ws") && (webSocketUri.Scheme != "wss"))
            {
                rootPage.NotifyUser("Error: WebSockets unterstützen nur ws:// und wss:// Schemas.", "error");
                return false;
            }

            uri = webSocketUri;

            return true;
        }
        // Verbindungsaufbau
        public async Task Connect()
        {
            try
            {                
                // Bereits verbunden?
                if (MessageWebSocket == null)
                {
                    // URI Validierung
                    Uri server;
                    if (!TryGetUri(UriString, out server))
                    {
                        return;
                    }

                    rootPage.NotifyUser("Verbinde mit: " + server, "status");

                    MessageWebSocket = new MessageWebSocket();
                    MessageWebSocket.Control.MessageType = SocketMessageType.Utf8;
                    MessageWebSocket.MessageReceived += MessageReceived;

                    // close event auf UI thread dispatchen, um asynchronen Zugriff auf MessageWebSocket zu ermöglichen
                    MessageWebSocket.Closed += async (senderSocket, args) => 
                    {
                        await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => Closed(senderSocket, args)); 
                    };
                    // Verbindung herstellen
                    await MessageWebSocket.ConnectAsync(server);
                    // Writer-Instanz zum Senden von Daten
                    MessageWriter = new DataWriter(MessageWebSocket.OutputStream);

                    rootPage.NotifyUser("Verbunden", "status");

                    ConnectionStatus = true;

                    string initiateStream = "request_data";
                    messageWriter.WriteString(initiateStream);

                    await messageWriter.StoreAsync();
                }
                else
                {
                    rootPage.NotifyUser("Bereits Verbunden", "status");
                }
            }
            catch (Exception ex)
            {
                if (ConnectionStatus == false || MessageWebSocket != null)
                {
                    MessageWebSocket.Dispose();
                    MessageWebSocket = null;
                }
                rootPage.NotifyUser("Fehler beim Verbindungsaufbau: " + ex.Message, "error");
                ConnectionStatus = false;

            }
        }
        // Eventhandler für Nachrichtenempfang
        private async void MessageReceived(MessageWebSocket sender, MessageWebSocketMessageReceivedEventArgs args)
        {
            try
            {
                using (DataReader reader = args.GetDataReader())
                {
                    reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                    // Nachricht aus dem Buffer lesen und am ? splitten
                    string read = reader.ReadString(reader.UnconsumedBufferLength);
                    string[] data = read.Split('?');

                    // Werte des Helligkeitssensor werden empfangen
                    if (data[0] == "HUM")
                    {
                        // Da UI-Elemente aktualisiert werden, Dispatcher für Marshalling verwenden
                        await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                            () =>
                        {
                            rootPage.DataStorage.UpdateHumiditySensor(data[1]);
                        });
                    }
                    // Werte des Temperatursensors werden empfangen
                    else if (data[0] == "TEMP")
                    {
                        await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                            () =>
                        {
                            rootPage.DataStorage.UpdateTemperatureSensor(data[1]);
                        });
                    }
                    // Andere Daten werden empfangen, gebe in der Statusanzeige aus
                    else
                    {
                        await rootPage.StatusBlockRef.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, 
                            () =>
                        {
                            rootPage.NotifyUser("Unbekannte Nachricht: " + read, "error");
                        });
                    }
                }
            }
            catch (Exception ex) // For debugging
            {
                WebErrorStatus status = WebSocketError.GetStatus(ex.GetBaseException().HResult);

                if (status == WebErrorStatus.Unknown)
                {
                    throw;
                }

                await rootPage.StatusBlockRef.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
                {
                    rootPage.NotifyUser("Error: " + status, "error");
                });

                if (status.ToString() == "ConnectionAborted")
                {
                    if (connectionStatus == true)
                    {
                        connectionStatus = false;
                    }
                    if (messageWebSocket != null)
                    {
                        messageWebSocket.Dispose();
                        messageWebSocket = null;
                    }
                }
            }
        }
        // Kann lokal oder vom Server ausgelöst werden über Close/Dispose()
        private async Task Closed(IWebSocket sender, WebSocketClosedEventArgs args)
        {
            await rootPage.StatusBlockRef.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                rootPage.NotifyUser("Error: " + args.Reason, "error");
            });

            if (connectionStatus == true)
            {
                connectionStatus = false;
            }

            if (messageWebSocket != null)
            {
                messageWebSocket.Dispose();
                messageWebSocket = null;
            }
        }
        public void CloseConnection()
        {
            try
            {
                if (messageWebSocket !=  null)
                {
                    rootPage.NotifyUser("Beende Verbindung", "status");
                    messageWebSocket.Close(1000, "Closed due to user request.");
                    messageWebSocket = null;
                    rootPage.NotifyUser("Verbindung beendet", "status");

                    if (rootPage.DataStorage.HumiditySensorStatus == "Online")
                    {
                        rootPage.DataStorage.SwitchHumiditySensorStatus("Offline");
                    }
                    if (rootPage.DataStorage.TemperatureSensorStatus == "Online")
                    {
                        rootPage.DataStorage.SwitchTemperatureSensorStatus("Offline");
                    }

                    if (connectionStatus == true)
                    {
                        connectionStatus = false;
                    }
                }
                else
                {
                    rootPage.NotifyUser("Keine aktive Verbindung vorhanden, stellen Sie zuerst eine Verbindung her", "error");
                }
            }
            catch (Exception ex)
            {
                WebErrorStatus status = WebSocketError.GetStatus(ex.GetBaseException().HResult);

                if (status == WebErrorStatus.Unknown)
                {
                    throw;
                }
                rootPage.NotifyUser("Error: " + status, "error");
            }
        }
        #endregion
    }
}