﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using DataModel;

namespace UniversalSensorClient
{
    // Einstellungen zur Verbindungssteuerung
    public sealed partial class Settings : Page
    {
        MainPage rootPage = MainPage.Current;
        public DataStorage DataStorage { get; set; }
        public Settings()
        {
            this.InitializeComponent();
            DataStorage = rootPage.DataStorage;
            textBoxSetIp.Text = rootPage.WsConnection.UriString;
            textBoxLowerHumidity.Text = DataStorage.LowerHumidity.ToString();
            textBoxUpperHumidity.Text = DataStorage.UpperHumidity.ToString();
            textBoxLowerTemperature.Text = DataStorage.LowerTemperature.ToString();
            textBoxUpperTemperature.Text = DataStorage.UpperTemperature.ToString();
            this.DataContext = DataStorage;
        }
        /// <summary>
        /// WebSocket-Verbindungsaufbau
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void buttonConnect_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBlockSetIp.Text))
            {
                rootPage.NotifyUser("Please specify text to send", "error");
                return;
            }
            else if (rootPage.WsConnection.ConnectionStatus == true)
            {
                rootPage.NotifyUser("Verbindung bereits aktiv", "error");
            }
            else
            {
                // URL auslesen
                rootPage.WsConnection.UriString = textBoxSetIp.Text;
                // Connect() Funktion übernimmt den eigentlichen Verbindungsaufbau
                await rootPage.WsConnection.Connect();
            }
        }
        /// <summary>
        /// WebSocket-Verbindung beenden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCloseConnection_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rootPage.WsConnection.ConnectionStatus == true)
                {
                    rootPage.WsConnection.CloseConnection();
                }
                else
                {
                    rootPage.NotifyUser("Keine aktive Verbindung vorhanden, stellen Sie zuerst eine Verbindung her", "error");
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        /// <summary>
        /// Server-URL in lokalen App-Einstellungen speichern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveUri_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBlockSetIp.Text))
            {
                rootPage.NotifyUser("Bitte geben Sie eine URL ein!", "error");
                return;
            }
            else
            {
                rootPage.WsConnection.UriString = textBoxSetIp.Text;
                rootPage.localSettings.Values["server_url"] = rootPage.WsConnection.UriString;
            }
            
        }

        private void buttonSaveHumidity_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxLowerHumidity.Text) || String.IsNullOrEmpty(textBoxUpperHumidity.Text))
            {
                rootPage.NotifyUser("Bitte geben Sie Grenzwerte für die Helligkeit ein!", "error");
                return;
            }
            else
            {
                DataStorage.UpdateHumidityThresholds(Convert.ToDouble(textBoxLowerHumidity.Text), Convert.ToDouble(textBoxUpperHumidity.Text));                
            }
        }

        private void buttonSaveTemperature_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxLowerTemperature.Text) || String.IsNullOrEmpty(textBoxUpperTemperature.Text))
            {
                rootPage.NotifyUser("Bitte geben Sie Grenzwerte für die Temperatur ein!", "error");
                return;
            }
            else
            {
                DataStorage.UpdateTemperatureThresholds(Convert.ToDouble(textBoxLowerTemperature.Text), Convert.ToDouble(textBoxUpperTemperature.Text));
            }
        }
    }
}
